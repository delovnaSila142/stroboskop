# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://delovnaSila142@bitbucket.org/asistent/stroboskop.git
cd stroboskop

Naloga 6.2.3:
https://bitbucket.org/delovnaSila142/stroboskop/commits/be15794dfc68a31e9b84d1eccf80bb763dc680da

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/delovnaSila142/stroboskop/commits/ebee062b449ba39eeb931286b8bce4cbd8f66332

Naloga 6.3.2:
https://bitbucket.org/delovnaSila142/stroboskop/commits/cb27dc0d9be39c8ebf8d0261be1366ecb6143078

Naloga 6.3.3:
https://bitbucket.org/delovnaSila142/stroboskop/commits/972a522477fe28464cce5dad98fb742ceb7753fb

Naloga 6.3.4:
https://bitbucket.org/delovnaSila142/stroboskop/commits/0ccc1255ef497623b5c040e4d026c96f202441fb

Naloga 6.3.5:

git push origin izgled
git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
Spremembe sem pozabil uveljaviti.

Naloga 6.4.2:
https://bitbucket.org/delovnaSila142/stroboskop/commits/56c930a1c8a95e2d432d3ae325ba6f308cc4d882

Naloga 6.4.3:
https://bitbucket.org/delovnaSila142/stroboskop/commits/aa797b934d3f0e11cbb284393ba1991c0981890a

Naloga 6.4.4:
https://bitbucket.org/delovnaSila142/stroboskop/commits/87ba8258837ee18c19cef3cb5f5797b9d6260a01